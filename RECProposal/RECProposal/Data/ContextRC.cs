﻿using Microsoft.EntityFrameworkCore;
using RECProposal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RECProposal.Data
{
    public class ContextRC : DbContext 
    {
        public DbSet<Cidade> Cidades { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Cotacao> Cotacoes { get; set; }
        public DbSet<Endereco> Endereco { get; set; }
        public DbSet<Estado> Estado { get; set; }
        public DbSet<Fornecedores> Fornecedores { get; set; }
        public DbSet<LicitacaoFaturamento> LicitacaoFaturamentos { get; set; }
        public DbSet<PedidoCompra> PedidoCompras { get; set; }
        public DbSet<ProdutoCotacao> ProdutosCotacao { get; set; }
        public DbSet<ProdutoRC> ProdutosRC { get; set; }
        public DbSet<PropostaLicitacao> PropostaLicitacao { get; set; }
        public DbSet<PropostaLicitacaoItem> PropostaLicitacaoItem { get; set; }     

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlite("Data Source=DataseREC.db");
        //}

        public ContextRC(DbContextOptions<ContextRC> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder) {
            //builder.Entity<ProdutoRC>().HasData(
            //    new ProdutoRC { Apresentacao = "Dorflex 500mg caixa", QuantidadeEmbalagem = "10",   UnidadeMedida = "comp", Concentracao = "2" , DataCotacao = DateTime.Now, PrecoCompra  = 2, PrecoVendaMinimo = 5, PrincipioAtivo = "AAS", QuantidadeEstoqueRC  = 5},
            //    new ProdutoRC { Apresentacao = "Novalgina 50ml fraco", QuantidadeEmbalagem = "10", UnidadeMedida = "comp", Concentracao = "2", DataCotacao = DateTime.Now, PrecoCompra = 2, PrecoVendaMinimo = 5, PrincipioAtivo = "AAS", QuantidadeEstoqueRC = 5 },
            //    new ProdutoRC { Apresentacao = "Torsilax 500mg caixa", QuantidadeEmbalagem = "10", UnidadeMedida = "comp", Concentracao = "2", DataCotacao = DateTime.Now, PrecoCompra = 2, PrecoVendaMinimo = 5, PrincipioAtivo = "AAS", QuantidadeEstoqueRC = 5 },
            //    new ProdutoRC { Apresentacao = "Gelol 500mg caixa", QuantidadeEmbalagem = "10", UnidadeMedida = "comp", Concentracao = "2", DataCotacao = DateTime.Now, PrecoCompra = 2, PrecoVendaMinimo = 5, PrincipioAtivo = "AAS", QuantidadeEstoqueRC = 5 }
            //    );
        }
    }
}
//www.entityframeworktutorial.net/code-first/code-based-migration-in-code-first.aspx