﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RECProposal.Migrations
{
    public partial class Inicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Endereco",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Logradouro = table.Column<string>(nullable: true),
                    Numero = table.Column<string>(nullable: true),
                    Complemento = table.Column<string>(nullable: true),
                    Bairro = table.Column<string>(nullable: true),
                    Cep = table.Column<int>(nullable: false),
                    IdCidade = table.Column<int>(nullable: false),
                    IdEstado = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Endereco", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Estado",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<string>(nullable: true),
                    UF = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Estado", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Clientes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<string>(nullable: true),
                    RazaoSocial = table.Column<string>(nullable: true),
                    Documento = table.Column<string>(nullable: true),
                    IdEndereco = table.Column<string>(nullable: true),
                    StandardId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clientes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Clientes_Endereco_StandardId",
                        column: x => x.StandardId,
                        principalTable: "Endereco",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Fornecedores",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<string>(nullable: true),
                    RazaoSocial = table.Column<string>(nullable: true),
                    Documento = table.Column<string>(nullable: true),
                    IdEndereco = table.Column<string>(nullable: true),
                    StandardId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fornecedores", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Fornecedores_Endereco_StandardId",
                        column: x => x.StandardId,
                        principalTable: "Endereco",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cidades",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<int>(nullable: false),
                    IdEstado = table.Column<int>(nullable: false),
                    EstadoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cidades", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cidades_Estado_EstadoId",
                        column: x => x.EstadoId,
                        principalTable: "Estado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PropostaLicitacao",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    NumeroProcessoCliente = table.Column<string>(nullable: true),
                    TipoPregao = table.Column<string>(nullable: true),
                    DescricaoProposta = table.Column<string>(nullable: true),
                    DeclaracaoArquivoProposta = table.Column<string>(nullable: true),
                    DadosAdicionaisArquivoProposta = table.Column<string>(nullable: true),
                    DataAberturaEdital = table.Column<DateTime>(nullable: true),
                    DataVencimento = table.Column<DateTime>(nullable: true),
                    DataEntregaProposta = table.Column<DateTime>(nullable: true),
                    DataAtualizacao = table.Column<DateTime>(nullable: true),
                    IDCliente = table.Column<int>(nullable: false),
                    ClienteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropostaLicitacao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PropostaLicitacao_Clientes_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Clientes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cotacoes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    NumeroProcessoFornecedor = table.Column<string>(nullable: true),
                    DescricaoCotacao = table.Column<string>(nullable: true),
                    DataSolicitacao = table.Column<DateTime>(nullable: true),
                    DataVencimento = table.Column<DateTime>(nullable: true),
                    DataEntregaProposta = table.Column<DateTime>(nullable: true),
                    DataAtualizacao = table.Column<DateTime>(nullable: true),
                    DataCotacao = table.Column<DateTime>(nullable: true),
                    IDFornecedor = table.Column<int>(nullable: false),
                    FornecedorId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cotacoes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cotacoes_Fornecedores_FornecedorId",
                        column: x => x.FornecedorId,
                        principalTable: "Fornecedores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LicitacaoFaturamentos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DadosFaturamento = table.Column<string>(nullable: true),
                    IdCliente = table.Column<string>(nullable: true),
                    ClienteId = table.Column<int>(nullable: true),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataVencimento = table.Column<DateTime>(nullable: false),
                    DataUltimaEntrega = table.Column<DateTime>(nullable: true),
                    DataAtualizacao = table.Column<DateTime>(nullable: true),
                    IdCotacao = table.Column<string>(nullable: true),
                    CotoacaoVencedoraId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LicitacaoFaturamentos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LicitacaoFaturamentos_Clientes_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Clientes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LicitacaoFaturamentos_Cotacoes_CotoacaoVencedoraId",
                        column: x => x.CotoacaoVencedoraId,
                        principalTable: "Cotacoes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PedidoCompras",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ValorTotal = table.Column<decimal>(nullable: false),
                    DataPedido = table.Column<DateTime>(nullable: false),
                    DataVencimento = table.Column<DateTime>(nullable: true),
                    DataEntrega = table.Column<DateTime>(nullable: true),
                    DataAtualizacao = table.Column<DateTime>(nullable: true),
                    IdLicitacaoFaturamento = table.Column<int>(nullable: false),
                    LicitacaoFaturamentoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PedidoCompras", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PedidoCompras_LicitacaoFaturamentos_LicitacaoFaturamentoId",
                        column: x => x.LicitacaoFaturamentoId,
                        principalTable: "LicitacaoFaturamentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProdutosRC",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PrincipioAtivo = table.Column<string>(nullable: true),
                    UnidadeMedida = table.Column<string>(nullable: true),
                    Apresentacao = table.Column<string>(nullable: true),
                    Concentracao = table.Column<string>(nullable: true),
                    QuantidadeEmbalagem = table.Column<string>(nullable: true),
                    QuantidadeEstoqueRC = table.Column<int>(nullable: false),
                    IdUltimoFornecedor = table.Column<int>(nullable: false),
                    DataCotacao = table.Column<DateTime>(nullable: false),
                    PrecoCompra = table.Column<decimal>(nullable: false),
                    PrecoVendaMinimo = table.Column<decimal>(nullable: false),
                    LicitacaoFaturamentoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProdutosRC", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProdutosRC_LicitacaoFaturamentos_LicitacaoFaturamentoId",
                        column: x => x.LicitacaoFaturamentoId,
                        principalTable: "LicitacaoFaturamentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProdutoFornecedor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    IdFornecedor = table.Column<int>(nullable: false),
                    IdUltimaCotacao = table.Column<int>(nullable: false),
                    DataUltimaCotacao = table.Column<DateTime>(nullable: false),
                    DataValidadeCotacao = table.Column<DateTime>(nullable: false),
                    PrincipioAtivo = table.Column<string>(nullable: true),
                    ApresentacaoFornecedor = table.Column<string>(nullable: true),
                    Concentracao = table.Column<string>(nullable: true),
                    Quantidade = table.Column<string>(nullable: true),
                    PrecoFornecedor = table.Column<decimal>(nullable: false),
                    IdProdutoRC = table.Column<int>(nullable: false),
                    ProdutoRCId = table.Column<int>(nullable: true),
                    FornecedoresId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProdutoFornecedor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProdutoFornecedor_Fornecedores_FornecedoresId",
                        column: x => x.FornecedoresId,
                        principalTable: "Fornecedores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProdutoFornecedor_ProdutosRC_ProdutoRCId",
                        column: x => x.ProdutoRCId,
                        principalTable: "ProdutosRC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProdutosCotacao",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    IdFornecedor = table.Column<int>(nullable: false),
                    IdCotacao = table.Column<int>(nullable: false),
                    PrincipioAtivo = table.Column<string>(nullable: true),
                    ApresentacaoFornecedor = table.Column<string>(nullable: true),
                    Concentracao = table.Column<string>(nullable: true),
                    Quantidade = table.Column<string>(nullable: true),
                    PrecoFornecedor = table.Column<decimal>(nullable: false),
                    DataCotacao = table.Column<DateTime>(nullable: false),
                    IdProdutoRC = table.Column<int>(nullable: false),
                    ProdutoRCId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProdutosCotacao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProdutosCotacao_ProdutosRC_ProdutoRCId",
                        column: x => x.ProdutoRCId,
                        principalTable: "ProdutosRC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PropostaLicitacaoItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    IdPropostaLicitacao = table.Column<int>(nullable: false),
                    ItemNumero = table.Column<int>(nullable: false),
                    DescritivoCertame = table.Column<string>(nullable: true),
                    UnidadeCertame = table.Column<string>(nullable: true),
                    Apresentacao = table.Column<string>(nullable: true),
                    PrincipioAtivo = table.Column<string>(nullable: true),
                    Concentracao = table.Column<string>(nullable: true),
                    QuantidadeEmbalagem = table.Column<string>(nullable: true),
                    PrecoVencedor = table.Column<decimal>(nullable: false),
                    PrecoOfertado = table.Column<decimal>(nullable: false),
                    IdFornecedorItem = table.Column<int>(nullable: false),
                    IdProdutoRC = table.Column<int>(nullable: false),
                    ProdutoOfertadoRCId = table.Column<int>(nullable: true),
                    PropostaLicitacaoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropostaLicitacaoItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PropostaLicitacaoItem_ProdutosRC_ProdutoOfertadoRCId",
                        column: x => x.ProdutoOfertadoRCId,
                        principalTable: "ProdutosRC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PropostaLicitacaoItem_PropostaLicitacao_PropostaLicitacaoId",
                        column: x => x.PropostaLicitacaoId,
                        principalTable: "PropostaLicitacao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cidades_EstadoId",
                table: "Cidades",
                column: "EstadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Clientes_StandardId",
                table: "Clientes",
                column: "StandardId");

            migrationBuilder.CreateIndex(
                name: "IX_Cotacoes_FornecedorId",
                table: "Cotacoes",
                column: "FornecedorId");

            migrationBuilder.CreateIndex(
                name: "IX_Fornecedores_StandardId",
                table: "Fornecedores",
                column: "StandardId");

            migrationBuilder.CreateIndex(
                name: "IX_LicitacaoFaturamentos_ClienteId",
                table: "LicitacaoFaturamentos",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_LicitacaoFaturamentos_CotoacaoVencedoraId",
                table: "LicitacaoFaturamentos",
                column: "CotoacaoVencedoraId");

            migrationBuilder.CreateIndex(
                name: "IX_PedidoCompras_LicitacaoFaturamentoId",
                table: "PedidoCompras",
                column: "LicitacaoFaturamentoId");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoFornecedor_FornecedoresId",
                table: "ProdutoFornecedor",
                column: "FornecedoresId");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoFornecedor_ProdutoRCId",
                table: "ProdutoFornecedor",
                column: "ProdutoRCId");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutosCotacao_ProdutoRCId",
                table: "ProdutosCotacao",
                column: "ProdutoRCId");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutosRC_LicitacaoFaturamentoId",
                table: "ProdutosRC",
                column: "LicitacaoFaturamentoId");

            migrationBuilder.CreateIndex(
                name: "IX_PropostaLicitacao_ClienteId",
                table: "PropostaLicitacao",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_PropostaLicitacaoItem_ProdutoOfertadoRCId",
                table: "PropostaLicitacaoItem",
                column: "ProdutoOfertadoRCId");

            migrationBuilder.CreateIndex(
                name: "IX_PropostaLicitacaoItem_PropostaLicitacaoId",
                table: "PropostaLicitacaoItem",
                column: "PropostaLicitacaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cidades");

            migrationBuilder.DropTable(
                name: "PedidoCompras");

            migrationBuilder.DropTable(
                name: "ProdutoFornecedor");

            migrationBuilder.DropTable(
                name: "ProdutosCotacao");

            migrationBuilder.DropTable(
                name: "PropostaLicitacaoItem");

            migrationBuilder.DropTable(
                name: "Estado");

            migrationBuilder.DropTable(
                name: "ProdutosRC");

            migrationBuilder.DropTable(
                name: "PropostaLicitacao");

            migrationBuilder.DropTable(
                name: "LicitacaoFaturamentos");

            migrationBuilder.DropTable(
                name: "Clientes");

            migrationBuilder.DropTable(
                name: "Cotacoes");

            migrationBuilder.DropTable(
                name: "Fornecedores");

            migrationBuilder.DropTable(
                name: "Endereco");
        }
    }
}
