﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RECProposal.Models
{
    public class Fornecedores
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string RazaoSocial { get; set; }
        public string Documento { get; set; }

        [ForeignKey("IdEndereco")]
        public string IdEndereco { get; set; }
        public virtual Endereco Standard { get; set; }
        public ICollection<ProdutoFornecedor> Produtos { get; set; }
    }
}
