﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RECProposal.Models
{
    public class ProdutoRC
    {
        public int Id { get; set; }
        public string PrincipioAtivo { get; set; }
        public string UnidadeMedida { get; set; }
        public string Apresentacao { get; set; }
        public string Concentracao { get; set; }
        public string QuantidadeEmbalagem { get; set; }
        public int QuantidadeEstoqueRC { get; set; }
        public int IdUltimoFornecedor { get; set; }
        public DateTime DataCotacao { get; set; }
        public decimal PrecoCompra { get; set; }
        public decimal PrecoVendaMinimo { get; set; }
    }
}
