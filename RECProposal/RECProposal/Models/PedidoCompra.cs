﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RECProposal.Models
{
    public class PedidoCompra
    {
        public int Id { get; set; }
        public decimal ValorTotal { get; set; }
        public DateTime DataPedido { get; set; }
        public DateTime? DataVencimento { get; set; }
        public DateTime? DataEntrega { get; set; }
        public DateTime? DataAtualizacao { get; set; }
        [ForeignKey("IdCotacao")]
        public int IdLicitacaoFaturamento { get; set; }
        public virtual LicitacaoFaturamento LicitacaoFaturamento { get; set; }
    }
}
