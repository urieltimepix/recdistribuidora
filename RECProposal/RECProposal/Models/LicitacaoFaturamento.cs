﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RECProposal.Models
{
    public class LicitacaoFaturamento
    {
        public int Id { get; set; }
        public string DadosFaturamento { get; set; }
        [ForeignKey("IdCliente")]
        public string IdCliente { get; set; }
        public virtual Cliente Cliente { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataVencimento { get; set; }
        public DateTime? DataUltimaEntrega { get; set; }
        public DateTime? DataAtualizacao { get; set; }
        [ForeignKey("IdCotacao")]
        public string IdCotacao { get; set; }
        public virtual Cotacao CotoacaoVencedora { get; set; }
        public ICollection<ProdutoRC> Produtos { get; set; }
        public ICollection<PedidoCompra> PedidosRealizados { get; set; }
    }
}
