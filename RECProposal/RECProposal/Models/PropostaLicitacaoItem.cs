﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RECProposal.Models
{
    public class PropostaLicitacaoItem
    {
        public int Id { get; set; }
        public int IdPropostaLicitacao { get; set; }
        public int ItemNumero { get; set; }
        public string DescritivoCertame { get; set; }
        public string UnidadeCertame { get; set; }
        public string Apresentacao { get; set; }        
        public string PrincipioAtivo { get; set; }
        public string Concentracao { get; set; }
        public string QuantidadeEmbalagem { get; set; }
        public decimal PrecoVencedor { get; set; }
        public decimal PrecoOfertado { get; set; }
        public int IdFornecedorItem { get; set; }
        [ForeignKey("IdProdutoRC")]
        public int IdProdutoRC { get; set; }
        public virtual ProdutoRC ProdutoOfertadoRC { get; set; }

    }
}
