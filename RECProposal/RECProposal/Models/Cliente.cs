﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RECProposal.Models
{
    /// <summary>
    /// Dados dos clientes solicitantes dos processos de licitação e venda direta
    /// </summary>
    public class Cliente
    {
        public int Id { get; set; }        
        public string Nome { get; set; }
        public string RazaoSocial { get; set; }
        public string Documento { get; set; }

        [ForeignKey("IdEndereco")] 
        public string IdEndereco { get; set; }        
        public virtual Endereco Standard { get; set; }
    }
}
