﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RECProposal.Models
{
    public class PropostaLicitacao
    {

        public int Id { get; set; }
        public string NumeroProcessoCliente { get; set; }
        public string TipoPregao { get; set; }
        public string DescricaoProposta { get; set; }
        public string DeclaracaoArquivoProposta { get; set; }
        public string DadosAdicionaisArquivoProposta { get; set; }
        public DateTime? DataAberturaEdital { get; set; }
        public DateTime? DataVencimento { get; set; }
        public DateTime? DataEntregaProposta { get; set; }
        public DateTime? DataAtualizacao { get; set; }


        [ForeignKey("IDCliente")]
        public int IDCliente { get; set; }
        public virtual Cliente Cliente { get; set; }
        public ICollection<PropostaLicitacaoItem> Produtos { get; set; }
    }
}
