﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RECProposal.Models
{
    public class Cotacao
    {
        public int Id { get; set; }        
        public string NumeroProcessoFornecedor { get; set; }        
        public string DescricaoCotacao { get; set; }
        
        public DateTime? DataSolicitacao { get; set; }
        public DateTime? DataVencimento { get; set; }
        public DateTime? DataEntregaProposta { get; set; }
        public DateTime? DataAtualizacao { get; set; }
        public DateTime? DataCotacao { get; set; }

        [ForeignKey("IDFornecedor")]
        public int IDFornecedor { get; set; }
        public virtual Fornecedores Fornecedor { get; set; }
        //public  virtual ICollection<ProdutoCotacao> Produtos { get; set; }
    }
}
