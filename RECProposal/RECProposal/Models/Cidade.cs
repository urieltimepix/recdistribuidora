﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RECProposal.Models
{
    public class Cidade
    {
        public int Id { get; set; }
        public int Nome { get; set; }

        [ForeignKey("IdEstado")]
        public int IdEstado { get; set; }
        public virtual Estado Estado { get; set; }
    }
}
