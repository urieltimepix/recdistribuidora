﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RECProposal.Models
{
    public class ProdutoCotacao
    {
        public int Id { get; set; }
        public int IdFornecedor { get; set; } //Não é necessario carregar o relacionamento
        public int IdCotacao { get; set; } //Não é necessario carregar o relacionamento
        public string PrincipioAtivo { get; set; }
        public string ApresentacaoFornecedor { get; set; }
        public string Concentracao { get; set; }
        public string Quantidade { get; set; }
        public decimal PrecoFornecedor { get; set; }
        public DateTime DataCotacao { get; set; }
        [ForeignKey("IdProdutoRC")]
        public int IdProdutoRC { get; set; }
        public ProdutoRC ProdutoRC { get; set; }
        
    }
}
