import React, { Component } from 'react';
import { Container } from 'reactstrap';
import { NavMenu } from './NavMenu';
import { HeaderREC } from './HeaderREC/HeaderREC';
import { BarraLateral } from './BarraLateral/BarraLateral';
import { ProdutoRCList } from './Cadastro/ProdutoRCList';

export class Layout extends Component {
  static displayName = Layout.name;

  render () {
    return (
        <div>
            <HeaderREC />
            <BarraLateral/>
        <NavMenu />
        <Container>
          {this.props.children}
        </Container>
      </div>
    );
  }
}
