﻿import React, { Component } from "react";
import { Route } from 'react-router';
import { Link } from 'react-router-dom';

export class BarraLateral extends Component {
    render() {
        return (
            <aside className="main-sidebar sidebar-dark-primary elevation-4">

                <a href="index3.html" className="brand-link">
                    <img src="dist/img/ReCLogo.png" alt="AdminLTE Logo" className="brand-image " />
                    
                    <span className="brand-text font-weight-light"> Control</span>
                </a>

                <div className="sidebar">

                    {//<div className="user-panel mt-3 pb-3 mb-3 d-flex">
                    //    <div className="image">
                    //        <img src="dist/img/user2-160x160.jpg" className="img-circle elevation-2" alt="User Image" />
                    //    </div>
                    //    <div className="info">
                    //        <a href="#" className="d-block">Alexander Pierce</a>
                    //    </div>
                        //</div>
                    }


                    <nav className="mt-2">
                        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">


                            <li className="nav-item has-treeview menu-open">
                                <a href="#" className="nav-link active">
                                    <i className="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Painel RC
                <i className="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                
                            </li>

                            <li className="nav-item has-treeview">
                                <a href="#" className="nav-link">
                                    <i className="nav-icon fas fa-copy"></i>
                                    <p>
                                        Licitacoes
                <i className="fas fa-angle-left right"></i>
                                        <span className="badge badge-info right">6</span>
                                    </p>
                                </a>
                                <ul className="nav nav-treeview">
                                    <li className="nav-item">
                                        <a href="pages/layout/top-nav.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Editais</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="pages/layout/top-nav-sidebar.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Propostas LIC</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="pages/layout/boxed.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Resultados</p>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>

                            <li className="nav-item has-treeview">
                                <a href="local" className="nav-link">
                                    <i className="nav-icon fas fa-handshake"></i>
                                    <p>
                                        Pedidos
                                    </p>
                                </a>
                                <ul className="nav nav-treeview">
                                    <li className="nav-item">
                                        <a href="pages/layout/top-nav.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>LIC Aprovadas</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="pages/layout/top-nav-sidebar.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Compra Direta</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="pages/layout/boxed.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Resultados</p>
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li className="nav-item has-treeview">
                                <a href="local" className="nav-link">
                                    <i className="nav-icon fas fa-calculator"></i>
                                    <p>
                                        Cotações
                                    </p>
                                </a>
                                <ul className="nav nav-treeview">
                                    <li className="nav-item">
                                        <a href="pages/layout/top-nav.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Solicitar</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="pages/layout/top-nav-sidebar.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Acompanhamento</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="pages/layout/boxed.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Produtos</p>
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            {/*  <li className="nav-item has-treeview">
                                <a href="local" className="nav-link">
                                    <i className="nav-icon fas fa-medkit"></i>
                                    <p>
                                        Produtos RC
                                    </p>
                                </a>
                                <ul className="nav nav-treeview">
                                    <li className="nav-item">
                                        <a href="pages/layout/top-nav.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>LIC Aprovadas</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="pages/layout/top-nav-sidebar.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Compra Direta</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="pages/layout/boxed.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Resultados</p>
                                        </a>
                                    </li>

                                </ul>
                            </li>*/}

                            <li className="nav-item">
                                <a href="https://adminlte.io/docs/3.0" className="nav-link">
                                    <i className="nav-icon fas fa-file"></i>
                                    <p>Documentos</p>
                                </a>
                            </li>

                            <li className="nav-header">CADASTRO</li>

                            <li className="nav-item">

                                <a href="Cadastro/ProdutoRCList" className="nav-link">
                                    <i className="nav-icon fas fa-medkit"></i>
                                    <p>
                                        Produtos
                <span className="badge badge-info right">2</span>
                                    </p>
                                </a>
                            </li>

                            <li className="nav-item">
                                <a href="pages/calendar.html" className="nav-link">
                                    <i className="nav-icon fas fa-heartbeat"></i>
                                    <p>
                                        Clientes
                <span className="badge badge-info right">2</span>
                                    </p>
                                </a>
                            </li>

                            <li className="nav-item">
                                <a href="pages/calendar.html" className="nav-link">
                                    <i className="nav-icon fas fa-capsules"></i>
                                    <p>
                                        Fornecedores
                <span className="badge badge-info right">2</span>
                                    </p>
                                </a>
                            </li>

                      
                        </ul>
                    </nav>

                </div>

            </aside>
        )
    }


}