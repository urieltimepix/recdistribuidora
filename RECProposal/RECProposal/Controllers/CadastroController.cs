﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RECProposal.Data;
using RECProposal.Models;

namespace RECProposal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CadastroController : ControllerBase
    {
        private readonly ContextRC _context;

        public CadastroController(ContextRC context)
        {
            _context = context;
        }

        // GET: api/Cadastro
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProdutoRC>>> GetProdutosRC()
        {
            return await _context.ProdutosRC.ToListAsync();
        }

        // GET: api/Cadastro/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProdutoRC>> GetProdutoRC(int id)
        {
            var produtoRC = await _context.ProdutosRC.FindAsync(id);

            if (produtoRC == null)
            {
                return NotFound();
            }

            return produtoRC;
        }

        // PUT: api/Cadastro/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProdutoRC(int id, ProdutoRC produtoRC)
        {
            if (id != produtoRC.Id)
            {
                return BadRequest();
            }

            _context.Entry(produtoRC).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProdutoRCExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Cadastro
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ProdutoRC>> PostProdutoRC(ProdutoRC produtoRC)
        {
            _context.ProdutosRC.Add(produtoRC);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProdutoRC", new { id = produtoRC.Id }, produtoRC);
        }

        // DELETE: api/Cadastro/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProdutoRC>> DeleteProdutoRC(int id)
        {
            var produtoRC = await _context.ProdutosRC.FindAsync(id);
            if (produtoRC == null)
            {
                return NotFound();
            }

            _context.ProdutosRC.Remove(produtoRC);
            await _context.SaveChangesAsync();

            return produtoRC;
        }

        private bool ProdutoRCExists(int id)
        {
            return _context.ProdutosRC.Any(e => e.Id == id);
        }
    }
}
