﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RECProposal.Data;
using RECProposal.Models;

namespace RECProposal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidosCompraController : ControllerBase
    {
        private readonly ContextRC _context;

        public PedidosCompraController(ContextRC context)
        {
            _context = context;
        }

        // GET: api/PedidosCompra
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PedidoCompra>>> GetPedidoCompras()
        {
            return await _context.PedidoCompras.ToListAsync();
        }

        // GET: api/PedidosCompra/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PedidoCompra>> GetPedidoCompra(int id)
        {
            var pedidoCompra = await _context.PedidoCompras.FindAsync(id);

            if (pedidoCompra == null)
            {
                return NotFound();
            }

            return pedidoCompra;
        }

        // PUT: api/PedidosCompra/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPedidoCompra(int id, PedidoCompra pedidoCompra)
        {
            if (id != pedidoCompra.Id)
            {
                return BadRequest();
            }

            _context.Entry(pedidoCompra).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PedidoCompraExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PedidosCompra
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<PedidoCompra>> PostPedidoCompra(PedidoCompra pedidoCompra)
        {
            _context.PedidoCompras.Add(pedidoCompra);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPedidoCompra", new { id = pedidoCompra.Id }, pedidoCompra);
        }

        // DELETE: api/PedidosCompra/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PedidoCompra>> DeletePedidoCompra(int id)
        {
            var pedidoCompra = await _context.PedidoCompras.FindAsync(id);
            if (pedidoCompra == null)
            {
                return NotFound();
            }

            _context.PedidoCompras.Remove(pedidoCompra);
            await _context.SaveChangesAsync();

            return pedidoCompra;
        }

        private bool PedidoCompraExists(int id)
        {
            return _context.PedidoCompras.Any(e => e.Id == id);
        }
    }
}
