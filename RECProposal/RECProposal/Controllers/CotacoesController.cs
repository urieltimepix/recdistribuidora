﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RECProposal.Data;
using RECProposal.Models;

namespace RECProposal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CotacoesController : ControllerBase
    {
        private readonly ContextRC _context;

        public CotacoesController(ContextRC context)
        {
            _context = context;
        }

        // GET: api/Cotacoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Cotacao>>> GetCotacoes()
        {
            return await _context.Cotacoes.ToListAsync();
        }

        // GET: api/Cotacoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Cotacao>> GetCotacao(int id)
        {
            var cotacao = await _context.Cotacoes.FindAsync(id);

            if (cotacao == null)
            {
                return NotFound();
            }

            return cotacao;
        }

        // PUT: api/Cotacoes/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCotacao(int id, Cotacao cotacao)
        {
            if (id != cotacao.Id)
            {
                return BadRequest();
            }

            _context.Entry(cotacao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CotacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Cotacoes
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Cotacao>> PostCotacao(Cotacao cotacao)
        {
            _context.Cotacoes.Add(cotacao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCotacao", new { id = cotacao.Id }, cotacao);
        }

        // DELETE: api/Cotacoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Cotacao>> DeleteCotacao(int id)
        {
            var cotacao = await _context.Cotacoes.FindAsync(id);
            if (cotacao == null)
            {
                return NotFound();
            }

            _context.Cotacoes.Remove(cotacao);
            await _context.SaveChangesAsync();

            return cotacao;
        }

        private bool CotacaoExists(int id)
        {
            return _context.Cotacoes.Any(e => e.Id == id);
        }
    }
}
