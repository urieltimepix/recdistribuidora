﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RECProposal.Data;
using RECProposal.Models;

namespace RECProposal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LicitacoesController : ControllerBase
    {
        private readonly ContextRC _context;

        public LicitacoesController(ContextRC context)
        {
            _context = context;
        }

        // GET: api/Licitacoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PropostaLicitacao>>> GetPropostaLicitacao()
        {
            return await _context.PropostaLicitacao.ToListAsync();
        }

        // GET: api/Licitacoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PropostaLicitacao>> GetPropostaLicitacao(int id)
        {
            var propostaLicitacao = await _context.PropostaLicitacao.FindAsync(id);

            if (propostaLicitacao == null)
            {
                return NotFound();
            }

            return propostaLicitacao;
        }

        // PUT: api/Licitacoes/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPropostaLicitacao(int id, PropostaLicitacao propostaLicitacao)
        {
            if (id != propostaLicitacao.Id)
            {
                return BadRequest();
            }

            _context.Entry(propostaLicitacao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PropostaLicitacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Licitacoes
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<PropostaLicitacao>> PostPropostaLicitacao(PropostaLicitacao propostaLicitacao)
        {
            _context.PropostaLicitacao.Add(propostaLicitacao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPropostaLicitacao", new { id = propostaLicitacao.Id }, propostaLicitacao);
        }

        // DELETE: api/Licitacoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PropostaLicitacao>> DeletePropostaLicitacao(int id)
        {
            var propostaLicitacao = await _context.PropostaLicitacao.FindAsync(id);
            if (propostaLicitacao == null)
            {
                return NotFound();
            }

            _context.PropostaLicitacao.Remove(propostaLicitacao);
            await _context.SaveChangesAsync();

            return propostaLicitacao;
        }

        private bool PropostaLicitacaoExists(int id)
        {
            return _context.PropostaLicitacao.Any(e => e.Id == id);
        }
    }
}
